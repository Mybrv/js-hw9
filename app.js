/*Практичні завдання
1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
    Використайте 2 способи для пошуку елементів.
    Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center). */

const featureElement = document.querySelectorAll(".feature")
console.log(featureElement);
const featureClass = document.getElementsByClassName("feature")
console.log(featureClass);

for (let i = 0; i < featureElement.length; i++) {
    featureElement[i].style.textAlign = "center"
}

//2. Змініть текст усіх елементів h2 на "Awesome feature".

const titleText = document.querySelectorAll("h2")

for (let i = 0; i < titleText.length; i++) {
    titleText[i].innerText = "Awesome feature"
}

//3.Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

const featureTitle = document.querySelectorAll(".feature-title")

for (let i = 0; i < featureTitle.length; i++) {
    featureTitle[i].textContent += "!"
}