1. Опишіть своїми словами що таке Document Object Model (DOM)

DOM - структура веб-сторінки, де кожен її елемент є об'єктом, що дозволяє маніпулювати ними за допомогою js

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML - дозволяє надати текстовий контент елементу в обгортках з HTML розмітки

innerText - додає лише текст, перезаписуючи вже існуючий, та не дає можливості передати в елемент розмітку.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
По іd: document.getElementById('elementId')
По класу: document.getElementsByClassName('className');
По тегу: document.getElementsByTagName('tagName');
По CSS селектору: document.querySelector('.className'); // повертає перший елемент з відповідним селектором
   var elements = document.querySelectorAll('.className'); // повертає всі елементи з відповідним селектором

мабуть, найбільш гнучкий спосіб - querySelector

4. Яка різниця між nodeList та HTMLCollection?
   NodeList: Може містити будь-які типи вузлів (элементи, текстові вузли, коментарі и т.д.).
   HTMLCollection: містить тільки элементи HTML.